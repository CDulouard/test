# Test

A simple project to train CI/CD, OOP and TDD.

## Installation

Keep calm and clone the project.

## Usage

Read the doc!

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Authors

* **Clement Dulouard** - *Initial work* - [CDulouard](https://github.com/CDulouard)

## License
[MIT](https://gitlab.com/CDulouard/test/-/blob/master/LICENSE)