def test_addition_2_plus_2_equals_4():
    # Given
    a = 2
    b = 2
    expected_result = 4
    # When
    result = a + b
    # Then
    assert result == expected_result
